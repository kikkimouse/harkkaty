/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;
 
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import static java.util.Date.parse;
import java.util.ResourceBundle;
import java.util.logging.Level;
import static java.util.logging.Level.parse;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.AccessibleAttribute;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


/**
 *
 * @author anuvanhanen
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private ComboBox<String> objectComboBox;
    private ObservableList<String> objectComboBoxdata = FXCollections.observableArrayList();
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private Button deleteButton;
    @FXML
    private Button createButton;
    @FXML
    private CheckBox firstclassBox;
    @FXML
    private CheckBox secondclassBox;
    @FXML
    private CheckBox thirdclassBox;  
    @FXML
    private CheckBox fragileButton;
    @FXML
    private Button createObjectButton;
    
    int priority = 0;
    @FXML
    private ComboBox<SmartPost> startCityBox;
    private ObservableList<String> startcitylist = FXCollections.observableArrayList();
    
    private ObservableList<String> cities = FXCollections.observableArrayList();
    @FXML
    private ComboBox<SmartPost> targetCityBox;
    private ObservableList<String> targetcitylist = FXCollections.observableArrayList();
    
    
    SmartPostParser parser = new SmartPostParser();
    public static ArrayList cityList;
    @FXML
    private Label helpLabel;
    @FXML
    private AnchorPane infoButton;
    
    //warehouse W;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        /*Adding items into combobox*/
        Package.newItem("Antiikkinenlasipullo", 314, 2, true);
        Package.newItem("Jalkapallo", 5100, 3, false);
        Package.newItem("Laatikollinen konvehteja", 12000, 50, false);
        Package.newItem("micro-simkortti", (float) 0.1, (float) 0.01, false);
        for (Item itemTemp : Package.itemList) {
            objectComboBoxdata.add(itemTemp.name);
        }      
        objectComboBox.setItems(objectComboBoxdata);
        
        try {
            parser.parsePost();
        } catch (Exception ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //cityList = parser.getPost();
        cityList = FXMLWebViewController.chosen;
        startCityBox.getItems().addAll(cityList);
        targetCityBox.getItems().addAll(cityList);
        
        /*
        for (SmartPost smartPost : FXMLWebViewController.cityList ){
            cities.add(smartPost.getAddress() + ", " + smartPost.getCode() + " " + smartPost.getCity());
        }
        startCityBox.setItems(cities);
        
        targetCityBox.setItems(cities);*/
        
        //W.getList().add.(cityComboBox.getValue());
        /*
        for (SmartPost tempSmartPost : SmartPostController.smartPostList) {
            if(!startcitylist.contains(tempSmartPost.getCity())){
                startcitylist.add(tempSmartPost.getAddress() + ", " + tempSmartPost.getCode() + " " + tempSmartPost.getCity());
            }
            if(!targetcitylist.contains(tempSmartPost.getCity())){
                targetcitylist.add(tempSmartPost.getAddress() + ", " + tempSmartPost.getCode() + " " + tempSmartPost.getCity());
            }
        }      
        startCityBox.setItems(startcitylist);
        targetCityBox.setItems(targetcitylist);*/
        
        //startCityBox.getItems().addAll(W.getList());
        //targetCityBox.getItems().addAll(W.getList());
    }    

     @FXML
    private void createNewObject(ActionEvent event) {
        
        /*Get variables from field*/
        try {
            String n = nameField.getText();
            float s = Float.parseFloat(sizeField.getText());
            float w = Float.parseFloat(massField.getText());
            boolean f = fragileButton.isSelected();
            /*New item*/
            Package.newItem(n, s, w, f);

            /*Add new item to list*/
            objectComboBox.getItems().clear();
            for (Item itemTemp : Package.itemList) {
                objectComboBoxdata.add(itemTemp.name);
            }
            objectComboBox.setItems(objectComboBoxdata);
            sizeField.clear();
            nameField.clear();
            massField.clear();
            fragileButton.disarm();
        } catch (NumberFormatException e) {
            helpLabel.setText("Size or mass is not number.");
        }
    }
    
    @FXML
    private void chooseObject(ActionEvent event) {
        //if(objectComboBox.getValue().size < 400) {
            //firstclassBox.setDisable(true);
            //thirdclassBox.setDisable(true);
        
        //}
        //else if (objectComboBox.getValue().size > 400 && objectComboBox.getValue().size < 1000) {
            //thirdclassBox.setDisable(true);
            //secondclassBox.setDisable(true);
        
        //else {
            
        //}
    }


    @FXML
    private void deleteAction(ActionEvent event) {
    }

    public ArrayList getCoordinantsForPath(SmartPost startCity, SmartPost endCity) {
        ArrayList<Float> path = new ArrayList<>();
        float iStLat = 0, iStLon = 0, iEnLat = 0, iEnLon = 0;
        SmartPostParser parse = new SmartPostParser();
        try {
            parse.parsePost();
        } catch (Exception ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (SmartPost tempSmartPost : parse.getPost()) {
            //tempSmartPost = smartPostList;
            //String tempCity = tempSmartPost.getAddress() + ", " + tempSmartPost.getCode() + " " + tempSmartPost.getCity();
            String tempCity = tempSmartPost.toString();
            if(tempCity.equals(startCity.toString())){
                iStLat = tempSmartPost.getLat();
                iStLon = tempSmartPost.getLng();
            }
            if(tempCity.equals(endCity.toString())){
                iEnLat = tempSmartPost.getLat();
                iEnLon = tempSmartPost.getLng();
            }
        }              
     
        path.add(iStLat);
        path.add(iStLon);
        path.add(iEnLat);
        path.add(iEnLon);
       
        return path;
    }
    
    @FXML
    private void createAction(ActionEvent event) {
        
        if (objectComboBox != null){
            /*Item is based on combobox and priority is based on radio buttons*/
            String tempSelectedItem = (String) objectComboBox.getSelectionModel().getSelectedItem();
            SmartPost startcitySelected =  startCityBox.getSelectionModel().getSelectedItem();
            SmartPost endcitySelected = targetCityBox.getSelectionModel().getSelectedItem();
            
            ArrayList<Float> path  = getCoordinantsForPath(startcitySelected, endcitySelected);
            System.out.println(path.toString());
            String info = Storage.newPackage(tempSelectedItem, priority, path);
            helpLabel.setText(info);
            
            double distance = distance(path.get(0), path.get(1), path.get(2), path.get(3),"K");
            System.out.println("distance: " + distance);
 
            //String info = Storage.newPackage(tempSelectedItem, priority, path);
            //packageinfo.setText(info);
    
        }
        
    }

    private static double distance(float lat1, float lon1, float lat2, float lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}

		return (dist);
	}    
        
        /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts decimal degrees to radians						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
    private void startWebView(ActionEvent event) {
        
        try {
            Stage webview = new Stage();
        
            Parent page = FXMLLoader.load(getClass().getResource("FXMLWebView.fxml"));
            
            Scene scene = new Scene(page);

            webview.setScene(scene);
            webview.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void firstclassAction(ActionEvent event) {
        priority = 1;   
    }

    @FXML
    private void secondclassAction(ActionEvent event) {
        priority = 2;
    }

    @FXML
    private void thirdclassAction(ActionEvent event) {
        priority = 3;
    }


    @FXML
    private void fragileButtonAction(ActionEvent event) {
        /*if (fragileButton.isSelected()){
            firstclassBox.setDisable(true);
            thirdclassBox.setDisable(true);
        
        }
        else {
                firstclassBox.setDisable(false);
                thirdclassBox.setDisable(false);
                }
        
        */
        }

    


    
}

   

