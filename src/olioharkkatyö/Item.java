/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;
 
/**
 
 * @author Nidal
 */
public class Item {
    protected float weight;
    protected float size;
    protected String name;
    protected boolean feature;
      
    public Item(String n, float s, float w, boolean f){
        weight = w;
        size = s;
        name = n;
        feature = f;   
    } 
    
    public float getWeight() {
        return weight;
    }
    
    public float getSize() {
        return size;
        
    }public String getName() {
        return name;
    }
    
    public boolean getFeature() {
        return feature;
    }
}

/*
class Antiikkilasipullo extends Item {
        
        public Antiikkilasipullo() {
            super("Antiikkinenlasipullo", 314, 2 , true);
        }
         @Override
        public String toString(){
        return name;
    }
}
class Jalkapallo extends Item {
    

        public Jalkapallo() {
            super("Jalkapallo", 5100, 3, false);
        }
         @Override
        public String toString(){
        return name;
    }
    }*/