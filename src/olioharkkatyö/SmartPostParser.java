/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


 
 

public class SmartPostParser {
    
    ArrayList<SmartPost> list = new ArrayList<SmartPost>();
    SmartPost post;
    
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private float lat;
    private float lng;
    
    private Document doc;
    
    public void parsePost() throws ParserConfigurationException, Exception {
        
        try {
            
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String inputLine;
            String content = "";

            while((inputLine = in.readLine()) != null) {
                content += inputLine+"\n";
            }
            in.close();
        
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(new InputSource(new StringReader(content)));
                list = new ArrayList();

                NodeList nodes = doc.getElementsByTagName("place");

                for(int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    if(node.getNodeType() == Node.ELEMENT_NODE) {
                        Element e = (Element) node;

                        String c = e.getElementsByTagName("code").item(0).getTextContent();
                        String C = e.getElementsByTagName("city").item(0).getTextContent();
                        String add = e.getElementsByTagName("address").item(0).getTextContent();
                        String av = e.getElementsByTagName("availability").item(0).getTextContent();
                        String po = e.getElementsByTagName("postoffice").item(0).getTextContent();
                        Float lt = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
                        Float lg = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());

                         list.add(new SmartPost(c, C, add, av, po, lt, lg));
                    }
                }

            } catch (SAXException | IOException ex) {
                Logger.getLogger(SmartPostParser.class.getName()).log(Level.SEVERE, null, ex);
            } 
            
        } catch (MalformedURLException ex) {
                Logger.getLogger(SmartPostParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
                Logger.getLogger(SmartPostParser.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    } 
        
    
    
    public ArrayList<SmartPost> getPost() {
        return list;
    }
       
}

