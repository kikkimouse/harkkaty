/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;

import java.util.ArrayList;
 
/**
 *
 * 
 * @author Nidal
 */

public class Package extends Item {
    public static ArrayList<Item> itemList = new ArrayList();
    private Item item;
    private int priority;
    private ArrayList<Float> path = new ArrayList();
   
    public Package(String n, float s, float w, boolean f, int pPriority, ArrayList pPath){
        super(n,s,w,f);
        priority = pPriority;
        path = pPath;
    }
    public Item getItem() {
        return item;
    }
    public int getPriority() {
        return priority;
    }
    public ArrayList<Float> getPath() {
        return path;
    }
   
    public static void newItem(String pName, float pSize, float pMass, boolean pBreakable){
        itemList.add(new Item(pName, pSize, pMass, pBreakable));        
    }
   
}
