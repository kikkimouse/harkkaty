
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;

import java.util.ArrayList;
import org.w3c.dom.Document;
 
/**
 *
 * 
 * @author anuvanhanen
 */
public class SmartPost {
    
    ArrayList<SmartPost> cityList = new ArrayList<SmartPost>();
    
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private float lat;
    private float lng; 
    
    
    private Document doc;
    
    //private static SmartPost sp = null;
    //static public SmartPost sp = null;
    /* 
    static public SmartPost getInstance() throws Exception {
        if (sp == null) {
           sp = new SmartPost();
        } else {
            System.out.println("Sp allready exists.");
        }  
        return sp;
    }*/

    public SmartPost(String c, String C, String add, String av, String po, Float lt, Float lg) {
        
        code = c;
        city = C;
        address = add;
        availability = av;
        postoffice = po;
        lat = lt;
        lng = lg;
        
    }
    
    public String toString() {
        return city + ": " + postoffice;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getAvailability() {
        return availability;
    }
    
    public String getPostoffice() {
        return postoffice;
    }
    
    public float getLat() {
        return lat;
    }
    
    public float getLng() {
        return lng;
    }
    
}
