/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;
 
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 *
 * @author Nidal
 */
public class Storage {
    
    public static ArrayList<Package> packageList = new ArrayList();
    
    /* This function creates a new package.*/
    public static String newPackage(String pItemName, int pPriority , ArrayList<Float> pPath){
        
        /* Finds item from itemlist.*/
        Item newPackageItem = null;
        Storage storage = new Storage();
        float distance = storage.getDistanceBetweenTowns(pPath);
        
        for (int i = 0; i < Package.itemList.size(); i++){
            Item tempitem = Package.itemList.get(i);
            if(tempitem.name.equals(pItemName)){
                newPackageItem = tempitem;  
            }
        }
        
        if(pItemName == null){
            return "Valitse esine!";
        }
        if(pPriority == 0){
            return "Valitse pakettiluokka!";
        }
        for (int i = 0; i < pPath.size(); i++){
            if(pPath.get(i) == 0){
                return "Valitse kaupungit!";  
            }
        } 

        if (pPriority == 1 && distance > 150 && newPackageItem.feature){
            return "Liian pitkä matka ykkösluokan paketille! Valitse uusi pakettiluokka!";
        }
        if (pPriority == 2 && newPackageItem.size > 20){
            return "Liian iso esine kakkosluokan paketille! Valitse uusi pakettiluokka!";
        }
        if (pPriority == 3 && (newPackageItem.feature || newPackageItem.weight < 20)){
            return "Liian hauras esine kolmosluokan paketille! Valitse uusi pakettiluokka!";
        }        
                
        Package newStoragePackage = new Package(newPackageItem.name, newPackageItem.size, newPackageItem.weight, newPackageItem.feature, pPriority, pPath);
        packageList.add(newStoragePackage);
        
        return "Paketti luotu!";
        
        
        
    }
    
    private float getDistanceBetweenTowns(ArrayList<Float> pPath) {
        double earthRadius = 6371; //km
        double dLat = Math.toRadians(pPath.get(2) - pPath.get(0));
        double dLng = Math.toRadians(pPath.get(3) - pPath.get(1));
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(pPath.get(0))) * Math.cos(Math.toRadians(pPath.get(2))) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) Math.round(earthRadius * c);

        return dist;
    }
    
}
