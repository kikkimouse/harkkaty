/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioharkkatyö;
 

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;

/**
 * FXML Controller class
 *
 * @author anuvanhanen
 */
public class FXMLWebViewController implements Initializable {
    @FXML
    private WebView web;
    @FXML
    private ComboBox<SmartPost> cityComboBox;
    @FXML
    private Button smartPostButton;
    @FXML
    private Button packageButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button sendButton;
    
    public static ArrayList<SmartPost> cityList ;
    public static ArrayList<SmartPost> chosen = new ArrayList<SmartPost>();
    
    SmartPostParser parse = new SmartPostParser();
    
    DocumentBuilder dBuilder;
    SmartPost sp;
    @FXML
    private Button updateButton;
    
    private ComboBox<Package> objectComboBox;
    private ObservableList<Package> objectComboBoxdata = FXCollections.observableArrayList();
    @FXML
    private ComboBox<Package> packageComboBox;
    
    //warehouse W;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            parse.parsePost();
        } catch (Exception ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
         cityList = parse.getPost();
        cityComboBox.getItems().addAll(cityList);
       

        web.getEngine().load(getClass().getResource("index.html").toExternalForm()); 
        /*
        try {
            W = warehouse.getInstance();
        } catch (Exception ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        //W.getList().add.(cityComboBox.getValue());
    }    

    private void loadLocalFile(ActionEvent event) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }
    
    /*
    private void drawRoad(ActionEvent event) {
        web.getEngine().executeScript("document.goToLocation('Skinnarilankatu 34, 53850 Lappeenranta', 'Ei auki', 'pink')");
    
        ArrayList<Float> al = new ArrayList<>();
        
        al.add(61.0543675f);
        al.add(28.1832322f);
        al.add(61.498112f);
        al.add(23.7660095f);
        
        web.getEngine().executeScript("document.createPath("+ al + ", 'black', 1)");
    }*/

    @FXML
    private void loadLocalFile(MouseEvent event) {
    }

    @FXML
    private void chooseCity(ActionEvent event) {
    }

    @FXML
    private void addSmartPost(ActionEvent event) {
        
        String search = "";
        String info = "";
        String colour = "";
        
        sp = cityComboBox.getValue();
        chosen.add(sp);
        search = sp.getAddress() +", "+sp.getCode()+" "+sp.getCity();
        //System.out.println(search);
        info = sp.getPostoffice() +" "+ sp.getAvailability();
        //System.out.println(info);
        colour = "pink";

        web.getEngine().executeScript("document.goToLocation('"+search+"','"+info+"','"+colour+"')");
    }

    @FXML
    private void createPackage(ActionEvent event) {
        try {
            Stage webview = new Stage();
        
            Parent page = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

            Scene scene = new Scene(page);

            webview.setScene(scene);
            webview.show();
  
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void deletePaths(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void sendPackage(ActionEvent event) {
        /*
        ArrayList<Float> al = new ArrayList<>();
        
        al.add(61.0543675f);
        al.add(28.1832322f);
        al.add(61.498112f);
        al.add(23.7660095f);
        
        web.getEngine().executeScript("document.createPath("+ al + ", 'black', 1)");*/
        
        if(packageComboBox.getSelectionModel().getSelectedItem() != null){
            Package tempSeclectedPackage = (Package) packageComboBox.getSelectionModel().getSelectedItem(); 

            web.getEngine().executeScript("document.createPath(" + tempSeclectedPackage.getPath() + ", 'black'," + tempSeclectedPackage.getPriority() + ")");

            objectComboBoxdata.remove(tempSeclectedPackage);
            packageComboBox.setItems(objectComboBoxdata);
        }
        else{
            System.out.println("Valitse paketti!");
        }
    }

    public ComboBox<SmartPost> getCityComboBox() {
        return cityComboBox;
    }

    public void setCityComboBox(ComboBox<SmartPost> cityComboBox) {
        this.cityComboBox = cityComboBox;
    }

    @FXML
    private void updatePackage(ActionEvent event) {
        
        packageComboBox.getItems().clear();
        for (int i = 0; i < Storage.packageList.size(); i++) {
            Package tempPackage;
            objectComboBoxdata.add(Storage.packageList.get(i));
        }
        packageComboBox.setItems(objectComboBoxdata);
    }
       
}
